<%-- 
    Document   : JsonPage1
    Created on : Jun 1, 2015, 12:45:56 PM
    Author     : Ruben Suardi
--%>
<%@page import="java.sql.Statement"%>
<%@page import="java.util.List"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="com.ejemplo.json.modelo.Contacto"%>
<%@page import="com.ejemplo.repositories.BrandsRepository"%>
<%@page import="java.sql.Connection"%>
<%@page import="com.ejemplo.conexion.Conexion"%>
<%@page import="com.ejemplo.json.modelo.BrandsModel"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.google.gson.Gson" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<%
   Connection conn = null;
    String Driver = "org.postgresql.Driver";
    String User = "postgres";
    String Password = "admin";
    String URL = "jdbc:postgresql://localhost:5433/APP";
    ResultSet rs1 = null;
    Statement stm1 = null;
    String servicio = request.getParameter("Servicio");
    
    if (servicio.equals("Login")) {

        Gson gson = new Gson();

        
        ArrayList<BrandsModel> listBrands1 = new ArrayList<BrandsModel>();
        ArrayList<BrandsModel> listBrands2 = new ArrayList<BrandsModel>();
        try 
        {
            Class.forName(Driver);
            conn = DriverManager.getConnection(URL, User, Password);
            
            BrandsRepository br = new BrandsRepository();
            listBrands2 = br.getBrands();
            
           String query = "SELECT * FROM \"Brands\"";
            stm1 = conn.createStatement();
            rs1 = stm1.executeQuery(query);
            
            while (rs1.next()) 
            {
                BrandsModel bm = new BrandsModel();
                bm.setBrandID(Integer.parseInt(rs1.getString("BrandID")));
                bm.setName(rs1.getString("Name"));
                
                listBrands2.add(bm);
            }
        } catch (Exception ex) 
        {
            System.out.println("Desconectado");
            String message = ex.getMessage();
        }

        String json = gson.toJson(listBrands2);
        out.println(json);

    } else if (servicio.equals("Otro")) {

    }
    conn.close();
%>

