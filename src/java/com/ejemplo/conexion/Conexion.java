/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ejemplo.conexion;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Ruben Suardi
 */
public final class Conexion {

    public Connection conn = null;
    private String Driver = "org.postgresql.Driver";
    private String User = "postgres";
    private String Password = "admin";
    private String URL = "jdbc:postgresql://localhost:5433/APP";
    public ResultSet rs1 = null;
    public Statement stm1 = null;
    private static  Conexion instancia = null;


    public static synchronized Conexion getInstancia() 
    {
        if(instancia == null)
        {
            try{
            instancia = new Conexion();
            }catch(SQLException ex)
            { 
              ex.getMessage();
            }
        }
        return instancia;
    }
  
    public Conexion() throws SQLException {

        try {
            Class.forName(Driver);
            conn = DriverManager.getConnection(URL, User, Password);
        } catch (ClassNotFoundException | SQLException ex) {
            System.out.println(ex.getMessage());
            conn.close();
        }
    }
}

