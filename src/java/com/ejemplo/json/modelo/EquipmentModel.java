/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ejemplo.json.modelo;

/**
 *
 * @author Ruben Suardi
 */
public class EquipmentModel {
    
    public int EquipmentID;
    public String Equipment;

    public EquipmentModel() {
    }

    public EquipmentModel(int EquipmentID, String Equipment) {
        this.EquipmentID = EquipmentID;
        this.Equipment = Equipment;
    }

    public int getEquipmentID() {
        return EquipmentID;
    }

    public void setEquipmentID(int EquipmentID) {
        this.EquipmentID = EquipmentID;
    }

    public String getEquipment() {
        return Equipment;
    }

    public void setEquipment(String Equipment) {
        this.Equipment = Equipment;
    }
    
    
}
