/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ejemplo.json.modelo;

import java.util.Date;

/**
 *
 * @author Ruben Suardi
 */
public class CustomerModel {

    public int CustomerID;
    public String Name;
    public boolean IsActive;
    public Date CreateDate;
    public boolean IsEnterprises;
    public String Name2;
    public String LastName;
    public String LastName2;
    public String Phone;
    public String Phone2;
    public String Document;

    public CustomerModel() {

    }

    public CustomerModel(int CustomerID, String Name, boolean IsActive, Date CreateDate,
            boolean IsEnterprises, String Name2, String LastName, String LastName2, String Phone, String Phone2, String Document) {
        this.CustomerID = CustomerID;
        this.Name = Name;
        this.IsActive = IsActive;
        this.CreateDate = CreateDate;
        this.IsEnterprises = IsEnterprises;
        this.Name2 = Name2;
        this.LastName = LastName;
        this.LastName2 = LastName2;
        this.Phone = Phone;
        this.Phone2 = Phone2;
        this.Document = Document;

    }

    public int getCustomerID()
    {
        return CustomerID;
    }
    public void setCustomerID(int CustomerID)
    {
        this.CustomerID = CustomerID;
    }

    public Date getCreateDate() {
        return CreateDate;
    }

    public void setCreateDate(Date CreateDate) {
        this.CreateDate = CreateDate;
    }

    public boolean isIsEnterprises() {
        return IsEnterprises;
    }

    public void setIsEnterprises(boolean IsEnterprises) {
        this.IsEnterprises = IsEnterprises;
    }

    public String getName2() {
        return Name2;
    }

    public void setName2(String Name2) {
        this.Name2 = Name2;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String LastName) {
        this.LastName = LastName;
    }

    public String getLastName2() {
        return LastName2;
    }

    public void setLastName2(String LastName2) {
        this.LastName2 = LastName2;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String Phone) {
        this.Phone = Phone;
    }

    public String getPhone2() {
        return Phone2;
    }

    public void setPhone2(String Phone2) {
        this.Phone2 = Phone2;
    }

    public String getDocument() {
        return Document;
    }

    public void setDocument(String Document) {
        this.Document = Document;
    }
    public String getName()
    {
        return Name;
    }
    public void setName(String Name)
    {
        this.Name = Name;
    }
    public boolean getIsActive()
    {
        return IsActive;
    }
    public void setIsActive(boolean IsActive)
    {
        this.IsActive = IsActive;
    }
}

