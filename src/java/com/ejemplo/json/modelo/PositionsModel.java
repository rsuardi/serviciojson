/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ejemplo.json.modelo;

/**
 *
 * @author Ruben Suardi
 */
public class PositionsModel {
    
    public int PositionsID;
    public String Position;
    
    public PositionsModel()
    {
    
    }
    public PositionsModel(int PositionsID,String Position)
    {
        this.PositionsID = PositionsID;
        this.Position = Position;
    }

    public int getPositionsID() {
        return PositionsID;
    }

    public void setPositionsID(int PositionsID) {
        this.PositionsID = PositionsID;
    }

    public String getPosition() {
        return Position;
    }

    public void setPosition(String Position) {
        this.Position = Position;
    }
    
    
}
