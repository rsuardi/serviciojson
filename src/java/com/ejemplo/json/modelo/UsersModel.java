/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ejemplo.json.modelo;

import java.util.Date;

/**
 *
 * @author Ruben Suardi
 */
public class UsersModel {
    
    public String Logon;
    public String Name;
    public Date CreateDate;

    public UsersModel() {
    }

    public UsersModel(String Logon, String Name, Date CreateDate) {
        this.Logon = Logon;
        this.Name = Name;
        this.CreateDate = CreateDate;
    }

    public String getLogon() {
        return Logon;
    }

    public void setLogon(String Logon) {
        this.Logon = Logon;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public Date getCreateDate() {
        return CreateDate;
    }

    public void setCreateDate(Date CreateDate) {
        this.CreateDate = CreateDate;
    }
    
    
}
