/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ejemplo.json.modelo;

/**
 *
 * @author Ruben Suardi
 */
public class ModelosModel {
    
    public int ModelosID;
    public String Name;
    public int BrandID;

    public ModelosModel() {
    }

    public ModelosModel(int ModelosID, String Name, int BrandID) {
        this.ModelosID = ModelosID;
        this.Name = Name;
        this.BrandID = BrandID;
    }

    public int getModelosID() {
        return ModelosID;
    }

    public void setModelosID(int ModelosID) {
        this.ModelosID = ModelosID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public int getBrandID() {
        return BrandID;
    }

    public void setBrandID(int BrandID) {
        this.BrandID = BrandID;
    }
    
    
}
