/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ejemplo.json.modelo;

import java.util.Date;

/**
 *
 * @author Ruben Suardi
 */
public class VehiclesModel {
    
    public int VehiclesID;
    public boolean IsActive;
    public int ModelID;
    public String Color;
    public String Year;
    public String Placa;
    public String Vin;
    public Date CreateDate;

    public VehiclesModel() {
    }

    public VehiclesModel(int VehiclesID, boolean IsActive, int ModelID, String Color, String Year, String Placa, String Vin, Date CreateDate) {
        this.VehiclesID = VehiclesID;
        this.IsActive = IsActive;
        this.ModelID = ModelID;
        this.Color = Color;
        this.Year = Year;
        this.Placa = Placa;
        this.Vin = Vin;
        this.CreateDate = CreateDate;
    }

    public int getVehiclesID() {
        return VehiclesID;
    }

    public void setVehiclesID(int VehiclesID) {
        this.VehiclesID = VehiclesID;
    }

    public boolean isIsActive() {
        return IsActive;
    }

    public void setIsActive(boolean IsActive) {
        this.IsActive = IsActive;
    }

    public int getModelID() {
        return ModelID;
    }

    public void setModelID(int ModelID) {
        this.ModelID = ModelID;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String Color) {
        this.Color = Color;
    }

    public String getYear() {
        return Year;
    }

    public void setYear(String Year) {
        this.Year = Year;
    }

    public String getPlaca() {
        return Placa;
    }

    public void setPlaca(String Placa) {
        this.Placa = Placa;
    }

    public String getVin() {
        return Vin;
    }

    public void setVin(String Vin) {
        this.Vin = Vin;
    }

    public Date getCreateDate() {
        return CreateDate;
    }

    public void setCreateDate(Date CreateDate) {
        this.CreateDate = CreateDate;
    }
    
    
}
