/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ejemplo.json.modelo;

/**
 *
 * @author Ruben Suardi
 */
public class BrandsModel {
    
    public String Name;
    public int BrandID;
    
    public BrandsModel()
    {
        
    }
    public BrandsModel(String Name, int BrandID)
    {
        this.Name = Name;
        this.BrandID = BrandID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public int getBrandID() {
        return BrandID;
    }

    public void setBrandID(int BrandID) {
        this.BrandID = BrandID;
    }
    
}
