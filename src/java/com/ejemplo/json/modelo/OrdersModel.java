/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ejemplo.json.modelo;

import java.util.Date;

/**
 *
 * @author Ruben Suardi
 */
public class OrdersModel {
    
    public int OrdersID;
    public Date CreateDate;
    public Date AcetpDate;
    public Date FinishDate;
    public int CustomerID;
    public int VehicleID;
    public byte GasLevel;
    public boolean ArePiecesInBaul;
    public String Logon;
    public String Comments;

    public OrdersModel() {
    }

    public OrdersModel(int OrdersID, Date CreateDate, Date AcetpDate, Date FinishDate, int CustomerID, int VehicleID, byte GasLevel, boolean ArePiecesInBaul, String Logon, String Comments) {
        this.OrdersID = OrdersID;
        this.CreateDate = CreateDate;
        this.AcetpDate = AcetpDate;
        this.FinishDate = FinishDate;
        this.CustomerID = CustomerID;
        this.VehicleID = VehicleID;
        this.GasLevel = GasLevel;
        this.ArePiecesInBaul = ArePiecesInBaul;
        this.Logon = Logon;
        this.Comments = Comments;
    }

    public int getOrdersID() {
        return OrdersID;
    }

    public void setOrdersID(int OrdersID) {
        this.OrdersID = OrdersID;
    }

    public Date getCreateDate() {
        return CreateDate;
    }

    public void setCreateDate(Date CreateDate) {
        this.CreateDate = CreateDate;
    }

    public Date getAcetpDate() {
        return AcetpDate;
    }

    public void setAcetpDate(Date AcetpDate) {
        this.AcetpDate = AcetpDate;
    }

    public Date getFinishDate() {
        return FinishDate;
    }

    public void setFinishDate(Date FinishDate) {
        this.FinishDate = FinishDate;
    }

    public int getCustomerID() {
        return CustomerID;
    }

    public void setCustomerID(int CustomerID) {
        this.CustomerID = CustomerID;
    }

    public int getVehicleID() {
        return VehicleID;
    }

    public void setVehicleID(int VehicleID) {
        this.VehicleID = VehicleID;
    }

    public byte getGasLevel() {
        return GasLevel;
    }

    public void setGasLevel(byte GasLevel) {
        this.GasLevel = GasLevel;
    }

    public boolean isArePiecesInBaul() {
        return ArePiecesInBaul;
    }

    public void setArePiecesInBaul(boolean ArePiecesInBaul) {
        this.ArePiecesInBaul = ArePiecesInBaul;
    }

    public String getLogon() {
        return Logon;
    }

    public void setLogon(String Logon) {
        this.Logon = Logon;
    }

    public String getComments() {
        return Comments;
    }

    public void setComments(String Comments) {
        this.Comments = Comments;
    }
    
    
}
