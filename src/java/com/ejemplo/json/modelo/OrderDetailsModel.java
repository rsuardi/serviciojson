/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ejemplo.json.modelo;

/**
 *
 * @author Ruben Suardi
 */
public class OrderDetailsModel {
    
    public int OrderDetailsID;
    public int OrdersID;
    public int EquipmentID;
    public boolean ValueIn;
    public boolean ValueOut;

    public OrderDetailsModel() {
    }

    public OrderDetailsModel(int OrderDetailsID, int OrdersID, int EquipmentID, boolean ValueIn, boolean ValueOut) {
        this.OrderDetailsID = OrderDetailsID;
        this.OrdersID = OrdersID;
        this.EquipmentID = EquipmentID;
        this.ValueIn = ValueIn;
        this.ValueOut = ValueOut;
    }

    public int getOrderDetailsID() {
        return OrderDetailsID;
    }

    public void setOrderDetailsID(int OrderDetailsID) {
        this.OrderDetailsID = OrderDetailsID;
    }

    public int getOrderID() {
        return OrdersID;
    }

    public void setOrderID(int OrderID) {
        this.OrdersID = OrderID;
    }

    public int getEquipmentID() {
        return EquipmentID;
    }

    public void setEquipmentID(int EquipmentID) {
        this.EquipmentID = EquipmentID;
    }

    public boolean isValueIn() {
        return ValueIn;
    }

    public void setValueIn(boolean ValueIn) {
        this.ValueIn = ValueIn;
    }

    public boolean isValueOut() {
        return ValueOut;
    }

    public void setValueOut(boolean ValueOut) {
        this.ValueOut = ValueOut;
    }
    
    
}
