/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ejemplo.json.modelo;

/**
 *
 * @author Ruben Suardi
 */
public class CustomersVehiclesModel {

    public int CustomersVehiclesID;
    public int CustomerID;
    public int VehicleID;

    public CustomersVehiclesModel() {
        
    }

    public CustomersVehiclesModel(int CustomersVehiclesID, int CustomerID, int VehicleID) {
        this.CustomersVehiclesID = CustomersVehiclesID;
        this.CustomerID = CustomerID;
        this.VehicleID = VehicleID;
    }

    public int getCustomersVehiclesID() {
        return CustomersVehiclesID;
    }

    public void setCustomersVehiclesID(int CustomersVehiclesID) {
        this.CustomersVehiclesID = CustomersVehiclesID;
    }

    public int getCustomerID() {
        return CustomerID;
    }

    public void setCustomerID(int CustomerID) {
        this.CustomerID = CustomerID;
    }

    public int getVehicleID() {
        return VehicleID;
    }

    public void setVehicleID(int VehicleID) {
        this.VehicleID = VehicleID;
    }

}
