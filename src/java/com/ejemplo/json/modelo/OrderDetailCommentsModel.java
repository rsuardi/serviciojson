/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ejemplo.json.modelo;

/**
 *
 * @author Ruben Suardi
 */
public class OrderDetailCommentsModel {
    
    public int OrderDetailsID;
    public String Comment;

    public OrderDetailCommentsModel() {
    }

    public OrderDetailCommentsModel(int OrderDetailsID, String Comment) {
        this.OrderDetailsID = OrderDetailsID;
        this.Comment = Comment;
    }

    public int getOrderDetailsID() {
        return OrderDetailsID;
    }

    public void setOrderDetailsID(int OrderDetailsID) {
        this.OrderDetailsID = OrderDetailsID;
    }

    public String getComment() {
        return Comment;
    }

    public void setComment(String Comment) {
        this.Comment = Comment;
    }
    
    
}
