/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ejemplo.json.modelo;

/**
 *
 * @author Ruben Suardi
 */
public class PreferencesModel {
    
    public String Value;
    
    public PreferencesModel(){
    }
    public PreferencesModel(String Value)
    {
        this.Value = Value;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String Value) {
        this.Value = Value;
    }
    
    
}
