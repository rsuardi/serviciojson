package com.ejemplo.json.servlet;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.ejemplo.json.modelo.BrandsModel;
import com.ejemplo.json.modelo.CustomerModel;
import com.ejemplo.json.modelo.CustomersVehiclesModel;
import com.ejemplo.json.modelo.EquipmentModel;
import com.ejemplo.json.modelo.ModelosModel;
import com.ejemplo.json.modelo.OrderDetailCommentsModel;
import com.ejemplo.json.modelo.OrderDetailsModel;
import com.ejemplo.json.modelo.OrdersModel;
import com.ejemplo.json.modelo.PaymentsModel;
import com.ejemplo.json.modelo.PositionsModel;
import com.ejemplo.json.modelo.PreferencesModel;
import com.ejemplo.json.modelo.UsersModel;
import com.ejemplo.json.modelo.VehiclesModel;
import com.ejemplo.repositories.BrandsRepository;
import com.ejemplo.repositories.CustomerRepository;
import com.ejemplo.repositories.CustomersVehiclesRepository;
import com.ejemplo.repositories.EquipmentRepository;
import com.ejemplo.repositories.ModelosRepository;
import com.ejemplo.repositories.OrderDetailsCommentsRepository;
import com.ejemplo.repositories.OrderDetailsRepository;
import com.ejemplo.repositories.OrdersRepository;
import com.ejemplo.repositories.PaymentsRepository;
import com.ejemplo.repositories.PositionsRepository;
import com.ejemplo.repositories.PreferencesRepository;
import com.ejemplo.repositories.UsersRepository;
import com.ejemplo.repositories.VehiclesRepository;
import com.ejemplo.writingJson.ManageJson;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ruben Suardi
 */
@WebServlet(urlPatterns = {"/ServletJson"})
public class ServletJson extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws java.sql.SQLException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, SQLException, IOException {
        response.setContentType("text/json;charset=UTF-8");

        PrintWriter out = response.getWriter();

        String servicio = request.getParameter("Servicio");

        Gson gson = new Gson();

        switch (servicio) {
            case "getBrands":
                ArrayList<BrandsModel> listBrands = new ArrayList<>();
                try {
                    BrandsRepository brandsR = new BrandsRepository();
                    listBrands = brandsR.getBrands();
                } catch (Exception ex) {
                    System.out.println("Desconectado");
                    ex.getMessage();
                }
                String json = gson.toJson(listBrands);
                out.println(json);
                
                break;
            case "getCustomer":
                ArrayList<CustomerModel> listBrands1 = new ArrayList<>();
                try {
                    CustomerRepository customerR = new CustomerRepository();
                    listBrands1 = customerR.getCustomer();

                } catch (Exception ex) {
                    System.out.println("Desconectado");
                    System.out.println(ex.getMessage());
                }
                String json1 = gson.toJson(listBrands1);
                out.println(json1);

                break;
            case "getCustomersVehicles":
                ArrayList<CustomersVehiclesModel> listBrands2 = new ArrayList<>();
                try {
                    CustomersVehiclesRepository customerVehiclesR = new CustomersVehiclesRepository();
                    listBrands2 = customerVehiclesR.getCustomerVehicles();

                } catch (Exception ex) {
                    System.out.println("Desconectado");
                    System.out.println(ex.getMessage());
                }
                String json2 = gson.toJson(listBrands2);
                out.println(json2);
                
                break;
                
            case "getEquipment":
                ArrayList<EquipmentModel> listBrands3 = new ArrayList<>();
                try {
                    EquipmentRepository equipmentR = new EquipmentRepository();
                    listBrands3 = equipmentR.getEquipment();

                } catch (Exception ex) {
                    System.out.println("Desconectado");
                    System.out.println(ex.getMessage());
                }
                String json3 = gson.toJson(listBrands3);
                out.println(json3);
                                
                break;
            case "getModelos":
                ArrayList<ModelosModel> listBrands4 = new ArrayList<>();
                try {
                    ModelosRepository modelosR = new ModelosRepository();
                    listBrands4 = modelosR.getModelos();

                } catch (Exception ex) {
                    System.out.println("Desconectado");
                    System.out.println(ex.getMessage());
                }
                String json4 = gson.toJson(listBrands4);
                out.println(json4);

                break;
            case "getOrderDetailsComments":
                ArrayList<OrderDetailCommentsModel> listBrands5 = new ArrayList<>();
                try {
                    OrderDetailsCommentsRepository orderDetailsCommentsR = new OrderDetailsCommentsRepository();
                    listBrands5 = orderDetailsCommentsR.getOrderDetailsComments();

                } catch (Exception ex) {
                    System.out.println("Desconectado");
                    System.out.println(ex.getMessage());
                }
                String json5 = gson.toJson(listBrands5);
                out.println(json5);

                break;
            case "getOrderDetails":
                ArrayList<OrderDetailsModel> listBrands6 = new ArrayList<>();
                try {
                    OrderDetailsRepository orderDetailsR = new OrderDetailsRepository();
                    listBrands6 = orderDetailsR.getOrderDetails();

                } catch (Exception ex) {
                    System.out.println("Desconectado");
                    System.out.println(ex.getMessage());
                }
                String json6 = gson.toJson(listBrands6);
                out.println(json6);

                break;
            case "getOrders":
                ArrayList<OrdersModel> listBrands7 = new ArrayList<>();
                try {
                    OrdersRepository ordersR = new OrdersRepository();
                    listBrands7 = ordersR.getOrders();

                } catch (Exception ex) {
                    System.out.println("Desconectado");
                    System.out.println(ex.getMessage());
                }
                String json7 = gson.toJson(listBrands7);
                out.println(json7);

                break;
            case "getPayments":
                ArrayList<PaymentsModel> listBrands8 = new ArrayList<>();
                try {
                    PaymentsRepository paymentsR = new PaymentsRepository();
                    listBrands8 = paymentsR.getPayments();

                } catch (Exception ex) {
                    System.out.println("Desconectado");
                    System.out.println(ex.getMessage());
                }
                String json8 = gson.toJson(listBrands8);
                out.println(json8);

                break;
            case "getPositions":
                ArrayList<PositionsModel> listBrands9 = new ArrayList<>();
                try {
                    PositionsRepository positionsR = new PositionsRepository();
                    listBrands9 = positionsR.getPositions();

                } catch (Exception ex) {
                    System.out.println("Desconectado");
                    System.out.println(ex.getMessage());
                }
                String json9 = gson.toJson(listBrands9);
                out.println(json9);

                break;
            case "getPreferences":
                ArrayList<PreferencesModel> listBrands10 = new ArrayList<>();
                try {
                    PreferencesRepository preferencesR = new PreferencesRepository();
                    listBrands10 = preferencesR.getPreferences();

                } catch (Exception ex) {
                    System.out.println("Desconectado");
                    System.out.println(ex.getMessage());
                }
                String json10 = gson.toJson(listBrands10);
                out.println(json10);

                break;
            case "getUsers":
                ArrayList<UsersModel> listBrands11 = new ArrayList<>();
                try {
                    UsersRepository usersR = new UsersRepository();
                    listBrands11 = usersR.getUsers();

                } catch (Exception ex) {
                    System.out.println("Desconectado");
                    System.out.println(ex.getMessage());
                }
                String json11 = gson.toJson(listBrands11);
                out.println(json11);

                break;
            case "getVehicles":
                ArrayList<VehiclesModel> listBrands12 = new ArrayList<>();
                try {
                    VehiclesRepository vehiclesR = new VehiclesRepository();
                    listBrands12 = vehiclesR.getVehicles();

                } catch (Exception ex) {
                    System.out.println("Desconectado");
                    System.out.println(ex.getMessage());
                }
                String json12 = gson.toJson(listBrands12);
                out.println(json12); 

                break;
            default:
                out.println("<html>");
                out.println("<head>");
                out.println("<style>"
                        + "body {background-color:lightgray}\n"
                        + "h1   {color:blue}\n"
                        + "p    {color:green}");
                out.println("</style>");
                out.println("<title>");
                out.println("</title>");
                out.println("</head>");
                out.println("<body>");
                out.println("<h1>No tengo parametros asignados para traer un Json!</h1>");
                out.println("</body>");

                break;

        }
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ServletJson.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ServletJson.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
