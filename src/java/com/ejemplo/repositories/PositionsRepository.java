/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ejemplo.repositories;

import com.ejemplo.conexion.Conexion;
import com.ejemplo.json.modelo.PositionsModel;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ruben Suardi
 */
public class PositionsRepository {
    
    
    Conexion conexion;
    
    public PositionsRepository() throws SQLException
    {
        this.conexion = new Conexion();
    }
    
    public ArrayList<PositionsModel> getPositions() 
    {
        ArrayList listBrands1 = new ArrayList();
        try {
            String query = "SELECT * FROM \"Positions\"";
            conexion.stm1 = conexion.conn.createStatement();
            conexion.rs1 = conexion.stm1.executeQuery(query);
            
            while (conexion.rs1.next()) 
            {
                PositionsModel positions = new PositionsModel();
                positions.setPositionsID(Integer.parseInt(conexion.rs1.getString("PositionsID")));
                positions.setPosition(conexion.rs1.getString("Position"));
                
                listBrands1.add(positions);
            }

        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listBrands1;
    }
}
