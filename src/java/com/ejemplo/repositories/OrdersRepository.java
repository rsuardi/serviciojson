/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ejemplo.repositories;

import com.ejemplo.conexion.Conexion;
import com.ejemplo.json.modelo.OrdersModel;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ruben Suardi
 */
public class OrdersRepository {
    
    Conexion conexion;
    
    public OrdersRepository() throws SQLException
    {
        this.conexion = new Conexion();
    }
    
    
    public ArrayList<OrdersModel> getOrders() 
    {
        ArrayList listBrands1 = new ArrayList();
        try {
            String query = "SELECT * FROM \"Orders\"";
            conexion.stm1 = conexion.conn.createStatement();
            conexion.rs1 = conexion.stm1.executeQuery(query);
            
            while (conexion.rs1.next()) 
            {
                OrdersModel orders = new OrdersModel();
                orders.setOrdersID(Integer.parseInt(conexion.rs1.getString("OrdersID")));
                orders.setCreateDate(conexion.rs1.getDate("CreateDate"));
                orders.setAcetpDate(conexion.rs1.getDate("AceptDate"));
                orders.setFinishDate(conexion.rs1.getDate("FinishDate"));
                orders.setCustomerID(Integer.parseInt(conexion.rs1.getString("CustomersID")));
                orders.setVehicleID(Integer.parseInt(conexion.rs1.getString("VehiclessID")));
                orders.setGasLevel(conexion.rs1.getByte("GasLevel"));
                orders.setArePiecesInBaul(conexion.rs1.getBoolean("ArePiecesInBaul"));
                orders.setLogon(conexion.rs1.getString("Logon"));
                orders.setComments(conexion.rs1.getString("Comments"));
                
                listBrands1.add(orders);
            }

        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listBrands1;
    }
}
