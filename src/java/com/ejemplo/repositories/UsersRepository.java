/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ejemplo.repositories;

import com.ejemplo.conexion.Conexion;
import com.ejemplo.json.modelo.UsersModel;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ruben Suardi
 */
public class UsersRepository {
    
    
    Conexion conexion;
    
    public UsersRepository() throws SQLException
    {
        this.conexion = new Conexion();
                
    }
    
    public ArrayList<UsersModel> getUsers() 
    {
        ArrayList listBrands1 = new ArrayList();
        try {
            String query = "SELECT * FROM \"Users\"";
            conexion.stm1 = conexion.conn.createStatement();
            conexion.rs1 = conexion.stm1.executeQuery(query);
            
            while (conexion.rs1.next()) 
            {
                UsersModel users = new UsersModel();
                users.setLogon(conexion.rs1.getString("Logon"));
                users.setName(conexion.rs1.getString("Name"));
                users.setCreateDate(conexion.rs1.getDate("CreateDate"));
                
                listBrands1.add(users);
                
            }

        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listBrands1;
    }
}
