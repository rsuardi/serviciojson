/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ejemplo.repositories;

import com.ejemplo.conexion.Conexion;
import com.ejemplo.json.modelo.BrandsModel;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ruben Suardi
 */
public class BrandsRepository {
    
    Conexion conexion;

    public BrandsRepository() throws SQLException {
        this.conexion = new Conexion();
    }
    
   public ArrayList<BrandsModel> getBrands() 
    {
        ArrayList listBrands1 = new ArrayList();
        try {
            String query = "SELECT * FROM \"Brands\"";
            conexion.stm1 = conexion.conn.createStatement();
            conexion.rs1 = conexion.stm1.executeQuery(query);
            
            while (conexion.rs1.next()) 
            {
                BrandsModel bm = new BrandsModel();
                bm.setBrandID(Integer.parseInt(conexion.rs1.getString("BrandID")));
                bm.setName(conexion.rs1.getString("Name"));
                
                listBrands1.add(bm);
            }

        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listBrands1;
    }
}
