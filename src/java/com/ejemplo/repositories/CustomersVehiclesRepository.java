/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ejemplo.repositories;

import com.ejemplo.conexion.Conexion;
import com.ejemplo.json.modelo.CustomersVehiclesModel;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ruben Suardi
 */
public class CustomersVehiclesRepository {
    
    Conexion conexion;
    
    public CustomersVehiclesRepository() throws SQLException
    {
        this.conexion = new Conexion();
    }
    
    public ArrayList<CustomersVehiclesModel> getCustomerVehicles() 
    {
        ArrayList listBrands1 = new ArrayList();
        try {
            String query = "SELECT * FROM \"CustomersVehicles\"";
            conexion.stm1 = conexion.conn.createStatement();
            conexion.rs1 = conexion.stm1.executeQuery(query);
            
            while (conexion.rs1.next()) 
            {
                CustomersVehiclesModel cv = new CustomersVehiclesModel();
                cv.setCustomersVehiclesID(Integer.parseInt(conexion.rs1.getString("CustomersVehiclesID")));
                cv.setCustomerID(Integer.parseInt(conexion.rs1.getString("CustomerID")));
                cv.setVehicleID(Integer.parseInt(conexion.rs1.getString("VehicleID")));
                
                listBrands1.add(cv);
            }

        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listBrands1;
    }
}
