/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ejemplo.repositories;

import com.ejemplo.conexion.Conexion;
import com.ejemplo.json.modelo.ModelosModel;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ruben Suardi
 */
public class ModelosRepository {
    
    Conexion conexion;
    
    public ModelosRepository() throws SQLException
    {
        this.conexion = new Conexion();
    }
    
    public ArrayList<ModelosModel> getModelos() 
    {
        ArrayList listBrands1 = new ArrayList();
        try {
            String query = "SELECT * FROM \"Modelos\"";
            conexion.stm1 = conexion.conn.createStatement();
            conexion.rs1 = conexion.stm1.executeQuery(query);
            
            while (conexion.rs1.next()) 
            {
                ModelosModel m = new ModelosModel();
                m.setModelosID(Integer.parseInt(conexion.rs1.getString("ModelosID")));
                m.setName(conexion.rs1.getString("Name"));
                m.setBrandID(Integer.parseInt(conexion.rs1.getString("BrandID")));
                
                listBrands1.add(m);
            }

        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listBrands1;
    }
}
