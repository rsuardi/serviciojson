/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ejemplo.repositories;

import com.ejemplo.conexion.Conexion;
import com.ejemplo.json.modelo.OrderDetailsModel;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ruben Suardi
 */
public class OrderDetailsRepository {
   
    Conexion conexion;
    
    public OrderDetailsRepository() throws SQLException
    {
        this.conexion = new Conexion();
    }
    
     public ArrayList<OrderDetailsModel> getOrderDetails() 
    {
        ArrayList listBrands1 = new ArrayList();
        try {
            String query = "SELECT * FROM \"OrderDetails\"";
            conexion.stm1 = conexion.conn.createStatement();
            conexion.rs1 = conexion.stm1.executeQuery(query);
            
            while (conexion.rs1.next()) 
            {
                OrderDetailsModel od = new OrderDetailsModel();
                od.setOrderID(Integer.parseInt(conexion.rs1.getString("OrdersID")));
                od.setOrderDetailsID(Integer.parseInt(conexion.rs1.getString("OrderDetailsID")));
                od.setEquipmentID(Integer.parseInt(conexion.rs1.getString("EquipmentID")));
                od.setValueIn(conexion.rs1.getBoolean("ValueIn"));
                od.setValueOut(conexion.rs1.getBoolean("ValueOut"));
               
                listBrands1.add(od);
            }

        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listBrands1;
    }
}
