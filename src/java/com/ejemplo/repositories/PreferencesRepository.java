/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ejemplo.repositories;

import com.ejemplo.conexion.Conexion;
import com.ejemplo.json.modelo.PreferencesModel;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ruben Suardi
 */
public class PreferencesRepository {
    
    
    Conexion conexion;
    
    public PreferencesRepository() throws SQLException
    {
        this.conexion = new Conexion();
    }
    
    public ArrayList<PreferencesModel> getPreferences() 
    {
        ArrayList listBrands1 = new ArrayList();
        try {
            String query = "SELECT * FROM \"Preferences\"";
            conexion.stm1 = conexion.conn.createStatement();
            conexion.rs1 = conexion.stm1.executeQuery(query);
            
            while (conexion.rs1.next()) 
            {
                PreferencesModel preferences = new PreferencesModel();
                preferences.setValue(conexion.rs1.getString("Value"));
                
                listBrands1.add(preferences);
            }

        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listBrands1;
    }
}
