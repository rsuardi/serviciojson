/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ejemplo.repositories;

import com.ejemplo.conexion.Conexion;
import com.ejemplo.json.modelo.EquipmentModel;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ruben Suardi
 */
public class EquipmentRepository {
    
    Conexion conexion;
    
    public EquipmentRepository() throws SQLException
    {
        this.conexion = new Conexion();
    }
    
    public ArrayList<EquipmentModel> getEquipment() 
    {
        ArrayList listBrands1 = new ArrayList();
        try {
            String query = "SELECT * FROM \"Equipments\"";
            conexion.stm1 = conexion.conn.createStatement();
            conexion.rs1 = conexion.stm1.executeQuery(query);
            
            while (conexion.rs1.next()) 
            {
                EquipmentModel eq = new EquipmentModel();
                eq.setEquipmentID(Integer.parseInt(conexion.rs1.getString("EquipmentID")));
                eq.setEquipment(conexion.rs1.getString("Equipment"));
                
                listBrands1.add(eq);
            }

        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listBrands1;
    }
    
}
