/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ejemplo.repositories;

import com.ejemplo.conexion.Conexion;
import com.ejemplo.json.modelo.CustomerModel;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ruben Suardi
 */
public class CustomerRepository {
    
    Conexion conexion;
    public CustomerRepository() throws SQLException
    {
        this.conexion = new Conexion();
    }
    
    public ArrayList<CustomerModel> getCustomer() 
    {
        ArrayList listBrands1 = new ArrayList();
        try {
            String query = "SELECT * FROM \"Customers\"";
            conexion.stm1 = conexion.conn.createStatement();
            conexion.rs1 = conexion.stm1.executeQuery(query);
            
            while (conexion.rs1.next()) 
            {
                CustomerModel cm = new CustomerModel();
                cm.setCustomerID(Integer.parseInt(conexion.rs1.getString("CustomersID")));
                cm.setName(conexion.rs1.getString("Name"));
                cm.setName2(conexion.rs1.getString("Name2"));
                cm.setIsActive(conexion.rs1.getBoolean("IsActive"));
                cm.setCreateDate(conexion.rs1.getDate("CreateDate"));
                cm.setIsEnterprises(conexion.rs1.getBoolean("IsEnterprises"));
                cm.setLastName(conexion.rs1.getString("LastName"));
                cm.setLastName2(conexion.rs1.getString("LastName2"));
                cm.setPhone(conexion.rs1.getString("Phone"));
                cm.setPhone2(conexion.rs1.getString("Phone2"));
                cm.setDocument(conexion.rs1.getString("Document"));
                
                listBrands1.add(cm);
            }

        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listBrands1;
    }
}
