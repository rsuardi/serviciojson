/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ejemplo.repositories;

import com.ejemplo.conexion.Conexion;
import com.ejemplo.json.modelo.VehiclesModel;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ruben Suardi
 */
public class VehiclesRepository {
    
    Conexion conexion;
    
    public VehiclesRepository() throws SQLException
    {
        this.conexion = new Conexion();
    }
    
    public ArrayList<VehiclesModel> getVehicles() 
    {
        ArrayList listBrands1 = new ArrayList();
        try {
            String query = "SELECT * FROM \"Vehicles\"";
            conexion.stm1 = conexion.conn.createStatement();
            conexion.rs1 = conexion.stm1.executeQuery(query);
            
            while (conexion.rs1.next()) 
            {
                VehiclesModel vehicles = new VehiclesModel();
                vehicles.setVehiclesID(Integer.parseInt(conexion.rs1.getString("VehiclesID")));
                vehicles.setIsActive(conexion.rs1.getBoolean("IsActive"));
                vehicles.setModelID(Integer.parseInt(conexion.rs1.getString("ModelID")));
                vehicles.setColor(conexion.rs1.getString("Color"));
                vehicles.setYear(conexion.rs1.getString("Year"));
                vehicles.setPlaca(conexion.rs1.getString("Placa"));
                vehicles.setVin(conexion.rs1.getString("Vin"));
                vehicles.setCreateDate(conexion.rs1.getDate("CreateDate"));
                
                listBrands1.add(vehicles);
            }

        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listBrands1;
    }
}
